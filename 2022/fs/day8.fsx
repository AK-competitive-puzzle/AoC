// PART 1
// This problem can essentially be solved with two passes - top-left to bottom-right and reverse.
// The first pass establishes candidates and they are checked on the second pass.

// ??? How is this not a function in the stdlib ???
let enumerate seq = Seq.mapi (fun ix it -> ix, it) seq
let charToInt c = int c - int '0'

let findCandidates1 (lines: (char list) list) : (int * int) Set =
    // Max value of column
    let maxYs = Array.init lines.Head.Length (fun _ -> -1)
    // Stores coordinates of candidates
    let mutable candidates = Set.empty

    for y, row in enumerate lines do
        let mutable maxX = -1

        for x, value in enumerate row do
            let value = charToInt value

            if maxX < value || maxYs[x] < value then
                candidates <- candidates.Add(x, y)

            maxYs[x] <- max maxYs[x] value
            maxX <- max maxX value

    candidates

let processInput1 input =
    let mutable candidates = findCandidates1 input
    let reversed = List.map (Seq.rev >> List.ofSeq) input |> List.rev
    let otherCandidates = findCandidates1 reversed
    let ys = input.Length
    let xs = input.Head.Length

    let otherCandidates =
        Set.map (fun (rx, ry) -> (xs - rx - 1, ys - ry - 1)) otherCandidates

    for otherCandidate in otherCandidates do
        candidates <- candidates.Add otherCandidate

    candidates.Count

// PART 2
type Direction =
    | Left
    | Up
    | Right
    | Down

let updateCoords ((x, y): int * int) (direction: Direction) =
    match direction with
    | Left -> (x - 1, y)
    | Up -> (x, y + 1)
    | Right -> (x + 1, y)
    | Down -> (x, y - 1)

let findDistance (input: (char list) list) (x, y) direction =
    let ys = input.Length
    let xs = input.Head.Length
    let startValue = charToInt (input[y][x])

    let rec findDistance' (x, y) distance =
        let (x, y) = updateCoords (x, y) direction

        if not (x >= 0 && y >= 0 && x < xs && y < ys) then
            distance
        else
            let currentValue = charToInt (input[y][x])

            if currentValue >= startValue then
                distance + 1
            else
                findDistance' (x, y) (distance + 1)

    findDistance' (x, y) 0

let findMultipliedDistance input (x, y) =
    let directions = [ Left; Up; Right; Down ]

    List.map (fun dir -> findDistance input (x, y) dir) directions
    |> List.reduce (*)

let processInput2 input =
    Seq.mapi (fun y row -> (Seq.mapi (fun x _ -> (findMultipliedDistance input (x, y))) row) |> Seq.max) input
    |> Seq.max

let inFilePath = fsi.CommandLineArgs[1]
let input = System.IO.File.ReadLines inFilePath |> Seq.map List.ofSeq |> List.ofSeq

match fsi.CommandLineArgs[2] with
| "1" -> printfn "%d" (processInput1 input)
| "2" -> printfn "%d" (processInput2 input)
| _ -> failwith "Unsupported part nr"
