open System
open System.IO

module Item =
    type T = private Item of char

    let create char =
        if Char.IsAsciiLetter char then Some(Item char) else None


    let value (Item char) = char

    let getPriority (char: T) =
        let subValue =
            match char with
            | Item x when Char.IsAsciiLetterLower x -> int 'a'
            | Item x when Char.IsAsciiLetterUpper x -> int 'A' - 26
            | c -> failwith $"Unsupported item char: ${c}"

        (char |> value |> int) - subValue + 1

module Compartment =
    type T = Item.T array

    let fromCharArray charArray : T =
        let maybeCompartment = Array.map Item.create charArray

        if not (Array.contains None maybeCompartment) then
            Array.map Option.get maybeCompartment
        else
            failwith "Unsupported character"

    let fromString (string: string) = fromCharArray (string.ToCharArray())

    let findCommonItem (compartments: T list) =
        match compartments with
        | head :: rest -> head |> Seq.find (fun item -> Seq.forall (Seq.contains item) rest)
        | _ -> failwith $"Unexpected compartment group: %A{compartments}"

module Rucksack =
    type T = Compartment.T * Compartment.T

    let fromLine (line: string) =
        let len = line.Length
        let mid = len / 2
        let comp = Compartment.fromCharArray (line.ToCharArray(0, mid))
        let comp2 = Compartment.fromCharArray (line.ToCharArray(mid, (len - mid)))
        comp, comp2

    let findOffender (rucksack: T) =
        fst rucksack |> Seq.find (fun a -> snd rucksack |> Seq.contains a)

let inFilePath = fsi.CommandLineArgs[1]
let input = File.ReadLines inFilePath

match fsi.CommandLineArgs[2] with
| "1" ->
    let processLine = Rucksack.fromLine >> Rucksack.findOffender >> Item.getPriority
    input |> Seq.map processLine |> Seq.sum |> printfn "Answer: %A"
| "2" ->
    let chunks =
        List.chunkBySize 3 (input |> Seq.map Compartment.fromString |> Seq.toList)

    chunks
    |> List.map Compartment.findCommonItem
    |> Seq.map Item.getPriority
    |> Seq.sum
    |> printfn "Answer: %A"
| _ -> failwith "Unsupported part nr"
