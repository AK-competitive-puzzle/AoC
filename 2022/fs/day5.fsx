open System
open System.IO

type Stack = List<char>

type CrateMovementMode =
    | Single
    | Multiple

let getStackCountFromLine (line: string) = (line.Length + 1) / 4

let getNthCrateInRow (row: string) n =
    let i = 4 * (n + 1) - 3
    row[i]

let processLine (stackArray: List<char> array) line =
    for i in [ 0 .. stackArray.Length - 1 ] do
        let crate = getNthCrateInRow line i

        if crate <> ' ' then
            stackArray[i] <- crate :: stackArray[i]

let parseStackDefinition (stackDef: string) =
    let lines = stackDef.Split "\n"
    let stackCount = getStackCountFromLine lines[0]
    let stacks = Array.create stackCount []
    Array.rev lines |> Array.skip 1 |> Array.map (processLine stacks) |> ignore
    stacks

type Move = { Count: int; From: int; To: int }
let char2NumericInt char = int char - int '0'

// Supports multiple digits for Count, but only single digits for From and To
let parseMove (move: string) =
    let withoutMove = move[5..]
    let spacePos = withoutMove.IndexOf ' '
    let withoutMoveNumber = withoutMove[spacePos + 1 ..]

    { Count = (int withoutMove[.. spacePos - 1])
      From = (char2NumericInt withoutMoveNumber[5]) - 1
      To = (char2NumericInt withoutMoveNumber[10]) - 1 }

let processMove mode (stacks: Stack array) (moveStr: string) =
    if not (String.IsNullOrWhiteSpace moveStr) then
        let move = parseMove moveStr

        let movedSection = stacks[move.From][0 .. move.Count - 1]

        let movedSection =
            match mode with
            | Single -> List.rev movedSection
            | Multiple -> movedSection

        stacks[move.From] <- stacks[move.From][move.Count ..]
        stacks[move.To] <- movedSection @ stacks[move.To]

let processMoves mode (moveDefs: string) stacks =
    for moveDef in moveDefs.Split "\n" do
        processMove mode stacks moveDef

let firstLetterOfStack (stack: Stack) = stack[0]

let processInput mode (fullInput: string) =
    let split = fullInput.Split "\n\n"
    let stacks = parseStackDefinition split[0]
    processMoves mode split[1] stacks
    stacks |> Array.map firstLetterOfStack |> String

let inFilePath = fsi.CommandLineArgs[1]
let input = File.ReadAllText inFilePath

let mode =
    match fsi.CommandLineArgs[2] with
    | "1" -> CrateMovementMode.Single
    | "2" -> CrateMovementMode.Multiple
    | _ -> failwith "Unsupported part nr"

processInput mode input |> printfn "Answer: %A"
