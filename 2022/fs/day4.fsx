open System.IO

module Range =
    type T = int * int

    let fromString (string: string) : T =
        let ints = string.Split '-' |> Array.take 2 |> Array.map int
        ints.[0], ints.[1]

module RangePair =
    type T = Range.T * Range.T
    type FilterFunc = T -> bool

    let fromString (string: string) : T =
        let ranges = string.Split ',' |> Array.take 2 |> Array.map Range.fromString
        ranges.[0], ranges.[1]

    let firstContainsSecond first second =
        (fst first) <= (fst second) && (snd first) >= (snd second)

    let oneContainsTheOther rangePair =
        firstContainsSecond (fst rangePair) (snd rangePair)
        || firstContainsSecond (snd rangePair) (fst rangePair)

    let overlap (first, second) =
        (fst first <= fst second && snd first >= fst second)
        || (fst first <= snd second && snd first >= snd second)
        || oneContainsTheOther (first, second)

let inFilePath = fsi.CommandLineArgs[1]
let input = File.ReadLines inFilePath

let filterFunc: RangePair.FilterFunc =
    match fsi.CommandLineArgs[2] with
    | "1" -> RangePair.oneContainsTheOther
    | "2" -> RangePair.overlap
    | _ -> failwith "Unsupported part nr"

input
|> Seq.map RangePair.fromString
|> Seq.filter filterFunc
|> Seq.length
|> printfn "Answer: %A"
