open System
open System.IO

let inFilePath = fsi.CommandLineArgs[1]

let input = File.ReadAllText inFilePath

let lineNotBlank line = not (String.IsNullOrWhiteSpace line)

let sumGroup (groupStr: string) =
    groupStr.Split "\n" |> Seq.filter lineNotBlank |> Seq.map int |> Seq.sum

input.Split "\n\n" |> Seq.map sumGroup |> Seq.max |> printfn "Answer: %d"
