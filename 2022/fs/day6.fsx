open System.IO

let allCharsUnique (window: (int * char) array) =
    let onlyChars = Seq.map snd window
    let set = Set onlyChars
    Set.count set = window.Length

let processInput windowSize (input: string) =
    Seq.indexed input
    |> Seq.windowed windowSize
    |> Seq.find allCharsUnique
    |> Seq.last
    |> fst
    |> (+) 1

let inFilePath = fsi.CommandLineArgs[1]
let input = File.ReadAllText inFilePath

let windowSize =
    match fsi.CommandLineArgs[2] with
    | "1" -> 4
    | "2" -> 14
    | _ -> failwith "Unsupported part nr"

processInput windowSize input |> printfn "Answer: %A"
