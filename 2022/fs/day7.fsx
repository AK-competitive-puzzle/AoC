type FileNode = { Size: int }

type FSNode =
    | File of FileNode
    | Dir of DirNode

and DirNode =
    { mutable Children: Map<string, FSNode> }

type NamedDirNode = string * DirNode

type Line =
    | Cd of string
    | Ls
    | Dir
    | FileStat of int * string

let parseLine (line: string) =
    match line with
    | l when l.StartsWith "$ cd" -> Cd line[5..]
    | l when l.StartsWith "$ ls" -> Ls
    | l when l.StartsWith "dir" -> Dir
    | l when not (System.String.IsNullOrWhiteSpace l) ->
        let split = l.Split ' '
        FileStat(int split[0], split[1])
    | l -> failwith $"Unrecognized line type: %A{l}"

// Does not support ../../../ with multiple levels
let parsePath (path: string) (currentPath: List<string>) =
    let rev = List.rev currentPath

    let rev =
        if path = "/" then
            [ path ]
        else if path.StartsWith '/' then
            path.Split '/' |> Array.toList
        else if path = ".." then
            rev.Tail
        else
            path :: rev

    List.rev rev

let rec findNodeByPath (path: List<string>) (treeRoot: NamedDirNode) =
    // printfn $"Finding node by path: %A{path} with root: %A{treeRoot}"
    let rootName, root = treeRoot

    match path with
    | head :: nextPath :: tail when head = rootName ->
        let maybeNext = Map.tryFind nextPath root.Children

        match maybeNext with
        | Some(FSNode.Dir d) -> findNodeByPath (nextPath :: tail) (nextPath, d)
        | Some(File _)
        | None -> None
    | [ head ] when head = rootName -> Some(FSNode.Dir root)
    | [] -> failwith "empty path"
    | _ -> failwith "not found"


let rec findNodeByPathOrCreateEmptyDir path treeRoot =
    match findNodeByPath path treeRoot with
    | Some a -> a
    | None ->
        let parentPath = parsePath ".." path
        // printfn $"Parent path: %A{parentPath}"
        let parentNode = findNodeByPath parentPath treeRoot

        // Creates parent if it does not exist
        let parentNode =
            match parentNode with
            | None -> findNodeByPathOrCreateEmptyDir parentPath treeRoot
            | Some parentNode -> parentNode

        match parentNode with
        | FSNode.File _ -> failwith "Parent is a file lol"
        | FSNode.Dir parentDir ->
            let childNode = FSNode.Dir { Children = Map.empty }
            parentDir.Children <- parentDir.Children.Add(path[path.Length - 1], childNode)
            childNode

let rec calculateSize (node: FSNode) =
    match node with
    | FSNode.File file -> file.Size
    | FSNode.Dir dir -> Seq.map calculateSize (Map.values dir.Children) |> Seq.sum

let rec getFlatListOfDirSizes (root: FSNode) =
    match root with
    | FSNode.File _ -> []
    | FSNode.Dir dir ->
        let childDirSizes =
            Seq.map getFlatListOfDirSizes (Map.values dir.Children)
            |> Seq.concat
            |> Seq.toList

        [ calculateSize root ] @ childDirSizes

let processLines (input: List<string>) postProcessTree =
    let mutable currentPath = [ "/" ]
    let rootDirNode = { Children = Map.empty }
    let root = ("/", rootDirNode)

    for line in input do
        let parsedLine = parseLine line

        match parsedLine with
        | Cd newDir -> currentPath <- parsePath newDir currentPath
        // printfn $"Current path: %A{currentPath}"
        | FileStat(size, name) ->
            let curDir = findNodeByPathOrCreateEmptyDir currentPath root

            let curDir =
                match curDir with
                | File _ -> failwith "Current dir is a file???"
                | FSNode.Dir curDir -> curDir

            curDir.Children <- curDir.Children.Add(name, (FSNode.File { Size = size }))
        // These lines contain no valuable information
        | Ls
        | Dir -> ()

    // printfn $"State before final calculation: %A{root}"
    postProcessTree rootDirNode

let getDirSizeSumLessThanX x rootNode =
    let dirs = getFlatListOfDirSizes (FSNode.Dir rootNode)
    List.filter (fun value -> value <= x) dirs |> List.sum

let getSmallestSufficientDir minRequiredSpace totalCapacity rootNode =
    let spaceTaken = calculateSize (FSNode.Dir rootNode)
    let minRequiredDirSize = minRequiredSpace - (totalCapacity - spaceTaken)
    let dirs = getFlatListOfDirSizes (FSNode.Dir rootNode)
    List.filter (fun value -> value >= minRequiredDirSize) dirs |> List.min

let inFilePath = fsi.CommandLineArgs[1]
let input = System.IO.File.ReadLines inFilePath |> List.ofSeq

match fsi.CommandLineArgs[2] with
| "1" ->
    processLines input (getDirSizeSumLessThanX 100_000)
    |> printfn "Amount of space in dirs with <= 100_000: %A"
| "2" ->
    processLines input (getSmallestSufficientDir 30_000_000 70_000_000)
    |> printfn "Size of smallest dir that is large enough: %A"
| _ -> failwith "Unsupported part nr"
