open System.IO

type Move =
    | Rock
    | Paper
    | Scissors

let str2Move str =
    match str with
    | "A"
    | "X" -> Rock
    | "B"
    | "Y" -> Paper
    | "C"
    | "Z" -> Scissors
    | _ -> failwith $"Unrecognized move: %s{str}"

let move2MovePoints move =
    match move with
    | Rock -> 1
    | Paper -> 2
    | Scissors -> 3

type Round = { YourMove: Move; OpponentsMove: Move }

let line2Round (str: string) =
    let split = str.Split " "

    { YourMove = Seq.last split |> str2Move
      OpponentsMove = Seq.head split |> str2Move }

type RoundResult =
    | Win
    | Tie
    | Loss

let result2ResultPoints result =
    match result with
    | Win -> 6
    | Tie -> 3
    | Loss -> 0

let round2Points round =
    let resultPoints =
        match round.YourMove, round.OpponentsMove with
        | Paper, Rock -> Win
        | Rock, Scissors -> Win
        | Scissors, Paper -> Win
        | a, b when a = b -> Tie
        | _ -> Loss
        |> result2ResultPoints

    let movePoints = move2MovePoints round.YourMove

    resultPoints + movePoints

let filePath = fsi.CommandLineArgs[1]
let input = File.ReadLines filePath
let line2Points = line2Round >> round2Points
Seq.map line2Points input |> Seq.sum |> printfn "Answer: %d"
