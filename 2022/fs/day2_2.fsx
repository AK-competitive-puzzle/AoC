open System.IO

type Move =
    | Rock
    | Paper
    | Scissors

let str2Move str =
    match str with
    | "A" -> Rock
    | "B" -> Paper
    | "C" -> Scissors
    | _ -> failwith $"Unrecognized move: %s{str}"

let move2MovePoints move =
    match move with
    | Rock -> 1
    | Paper -> 2
    | Scissors -> 3

type RoundResult =
    | Win
    | Tie
    | Loss

let str2Result str =
    match str with
    | "X" -> Loss
    | "Y" -> Tie
    | "Z" -> Win
    | s -> failwith $"Unexpected result: %s{s}"

type Round =
    { Result: RoundResult
      OpponentsMove: Move }

let round2YourMove round =
    match round.Result, round.OpponentsMove with
    | Win, Rock -> Paper
    | Loss, Paper -> Rock
    | Win, Paper -> Scissors
    | Loss, Scissors -> Paper
    | Win, Scissors -> Rock
    | Loss, Rock -> Scissors
    | Tie, other -> other

let line2Round (str: string) =
    let split = str.Split " "

    { Result = Seq.last split |> str2Result
      OpponentsMove = Seq.head split |> str2Move }


let result2ResultPoints result =
    match result with
    | Win -> 6
    | Tie -> 3
    | Loss -> 0

let round2Points round =
    let resultPoints = result2ResultPoints round.Result
    let movePoints = round2YourMove round |> move2MovePoints
    resultPoints + movePoints

let filePath = fsi.CommandLineArgs[1]
let input = File.ReadLines filePath
let line2Points = line2Round >> round2Points
Seq.map line2Points input |> Seq.sum |> printfn "Answer: %d"
